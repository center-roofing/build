<?php
/**
 * JBZoo is universal CCK based Joomla! CMS and YooTheme Zoo component
 * @category   JBZoo
 * @author     smet.denis <admin@joomla-book.ru>
 * @copyright  Copyright (c) 2009-2012, Joomla-book.ru
 * @license    http://joomla-book.ru/info/disclaimer
 * @link       http://joomla-book.ru/projects/jbzoo JBZoo project page
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="filter_cols">
    <div class="width50">
        <?php echo  $this->renderPosition('left', array('style' => 'filter.block'));?>
    </div>
    <div class="width50">
        <?php echo  $this->renderPosition('right', array('style' => 'filter.block'));?>
    </div>
    <div class="clear clr"></div>
</div>
